package resources;
 
import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;
import data.User;
 
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
 
/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {
 
    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId){
        GroupChat groupChat;
        GroupChatDAO groupChatDAO = new GroupChatDAO();
 
        groupChat = groupChatDAO.getGroupChat(groupChatId);
 
        ArrayList<Message> messages = groupChatDAO.getGroupChatMessages(groupChatId);
        groupChat.setMessageList(messages);
 
 
        ArrayList<User> users = groupChatDAO.getGroupChatUsers(groupChatId);
        groupChat.setUserList(users);
 
        return groupChat;
    }
 
    /**
     * GET method to get all GroupChats for one user
     * @param userId of one user
     * @return ArrayList of GroupChat objects
     */
    @GET
    @Path("user/{userId}")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChatByUserId(@PathParam("userId") int userId){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.getGroupChatByUserId(userId);
    }
 
    /**
     * POST method to add new GroupChat
     * @param groupChat The GroupChat to be added
     * @return the newly created GroupChat
     */
    @POST
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupChat){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.addGroupChat(groupChat);
    }
 
    /**
     * GET method to get all Messages for a GroupChat
     * @param groupChatId Id of the GroupChat
     * @return Arraylist of Messages
     */
    @GET
    @Path("/{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<Message> getMessages(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.getGroupChatMessages(groupChatId);
    }
 
    /**
     * POST method to add new message to a GroupChat
     * @param groupChatId id of the chat to add it to
     * @param message the Message to add
     * @return the newly created Message
     */
    @POST
    @Path ("/{groupChatId}/message")
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.addMessage(groupChatId, message);
    }
}