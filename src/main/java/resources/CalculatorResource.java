package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public double calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll(" ","");

        //Splits the expression into an array of numbers and an array of operators
        String[] numbers = expressionTrimmed.split("[-+*/]");
        String[] operators0 = expressionTrimmed.split("[0-9]");
        String[] operators = resize(operators0);

        //Goes through all operators and handle calculation.
        double result = Double.parseDouble(numbers[0]);

        for(int i = 0; i < operators.length; i++) {
            if(operators[i].contains("+")) {
                result = sum(result, Integer.parseInt(numbers[i+1]));
            }
            else if(operators[i].contains("-")) { 
                result = subtraction(result, Integer.parseInt(numbers[i+1]));
            }
            else if(operators[i].contains("*")) {
                result = multiplication(result, Integer.parseInt(numbers[i+1]));
            }
            else if(operators[i].contains("/")) {
                result = division(result, Integer.parseInt(numbers[i+1]));
            }
            else{
                throw new IllegalArgumentException("Operator not found");
            }
        }

        return result;
    }

    /**
     * Method used to calculate a sum expression.
     * @param number1 : int
     * @param number2 : int
     * @return the answer as an int
     */
    public double sum(double number1, int number2){
        return number1 + (double) number2;
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param number1 : int
     * @param number2 : int
     * @return the answer as an int
     */
    public double subtraction(double number1, int number2){
        return number1 - (double) number2;
    }

    /**
     * Method used to calculate a multiplication expression.
     * @param number1 : int
     * @param number2 : int
     * @return the answer as an int
     */
    public double multiplication(double number1, int number2) { 
        return number1 * (double) number2;
    }

    /**
     * Method used to calculate a division expression.
     * @param number1 : int
     * @param number2 : int
     * @return the answer as an int
     */
    public double division(double number1, int number2){ 
        return number1 / (double) number2;
    }

    /**
     * Resizes array, to fix spilt issue
     * @param oldArray
     * @return New string array with no empty elements
     */
    private String[] resize(String[] oldArray) {
        int count = 0;
        for (String i : oldArray) {
            if (!i.equals("")) {
                count++;
            }
        }

        String[] newArray = new String[count];
        int index = 0;
        for (String i : oldArray) {
            if (!i.equals("")) {
                newArray[index++] = i;
            }
        }

        return newArray;
    }
}
