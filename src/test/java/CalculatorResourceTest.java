import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CalculatorResourceTest{
    
    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300 * 2 -4 /2";
        assertEquals(398, calculatorResource.calculate(expression), 0.001);

        expression = " 300 - 99 ";
        assertEquals(201, calculatorResource.calculate(expression), 0.001);

        expression = "12 * 3";
        assertEquals(36, calculatorResource.calculate(expression), 0.001);

        try{
            calculatorResource.calculate("Hva er 2+2");
            fail();
        }catch(IllegalArgumentException error){
            return;
        }
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals(400, calculatorResource.sum(300,100), 0);
        assertEquals(800, calculatorResource.sum(400, 400), 0);
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals(470, calculatorResource.subtraction(500,30), 0);
        assertEquals(799, calculatorResource.subtraction(800,1), 0);
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals(56, calculatorResource.multiplication(28,2), 0);
        assertEquals(100, calculatorResource.multiplication(10, 10), 0);
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        assertEquals(7, calculatorResource.division(21, 3), 0);
        assertEquals(5, calculatorResource.division(25, 5), 0);
    }
}
